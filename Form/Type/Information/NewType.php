<?php

namespace Nitra\InformationBundle\Form\Type\Information;

use Admingenerated\NitraInformationBundle\Form\BaseInformationType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewType extends BaseNewType
{
    protected $storeId;
    protected $options;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->storeId = isset($options['store_id']) ? $options['store_id'] : 0;
        parent::buildForm($builder, $options);
        if ($builder->has('informationCategory')) {
            $builder->add('informationCategory', 'document', array_merge($this->options['informationCategory'], array(
                'query_builder' => function (DocumentRepository $dr) {
                    $query = $dr->createQueryBuilder('NitraInformationBundle:InformationCategory')
                        ->field('store.$id')
                        ->equals(new \MongoId($this->storeId));
                    return $query;
                },
            )));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setOptional(array(
            'store_id',
        ));
    }

    protected function getFormOption($name, array $formOptions)
    {
        $this->options[$name] = $formOptions;
        return $formOptions;
    }
}