<?php

namespace Nitra\InformationBundle\Controller\InformationCategory;

use Admingenerated\NitraInformationBundle\BaseInformationCategoryController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function getQuery()
    {
        $query = $this->buildQuery();
        $session = $this->getRequest()->getSession();
        //выводим категории текущего магазина и категории не относящиеся ни к одному из магазинов
        $query->addOr($query->expr()->field('store.$id')->equals(new \MongoId($session->get('store_id'))));
        $query->addOr($query->expr()->field('store')->exists(false));

        $this->processSort($query);
        $this->processFilters($query);
        $this->processScopes($query);

        return $query;
    }
}