<?php

namespace Nitra\InformationBundle\Controller\InformationCategory;

use Admingenerated\NitraInformationBundle\BaseInformationCategoryController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    public function postSave(\Symfony\Component\Form\Form $form, $InformationCategory)
    {
        $session = $this->getRequest()->getSession();
        $storeId = $session->get('store_id');
        $mongoDbName = $this->container->getParameter('mongo_database_name');
        $storeHost   = $this->container->get('doctrine_mongodb.odm.document_manager')->getRepository('NitraStoreBundle:Store')->find($storeId)->getHost();
        $cache       = $this->getCache();
        $cache->delete($mongoDbName . '_nitra_nice_information_categories_urls_' . $storeHost);
        $cache->delete($mongoDbName . '_nitra_nice_informations_urls_' . $storeHost);
    }
    
    /**
     * @return \Doctrine\Common\Cache\ApcCache
     */
    private function getCache()
    {
        return $this->container->get('cache_apc');
    }
}