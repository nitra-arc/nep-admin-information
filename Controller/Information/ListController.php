<?php

namespace Nitra\InformationBundle\Controller\Information;

use Admingenerated\NitraInformationBundle\BaseInformationController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function getQuery()
    {
        $session = $this->getRequest()->getSession();
        // получаем информационные категории текущего магазина
        $infoCategories = $this->getDocumentManager()->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->field('store.$id')->equals(new \MongoId($session->get('store_id')))
            ->getQuery()->execute();

        $ids = array();
        foreach ($infoCategories as $infoCategory) {
            $ids[] = new \MongoId($infoCategory->getId());
        }

        $query = $this->buildQuery();

        // выводим статьи текущего магазина 
        $query->addAnd($query->expr()->field('informationCategory.$id')->in($ids));

        $this->processSort($query);
        $this->processFilters($query);
        $this->processScopes($query);

        return $query;
    }
}