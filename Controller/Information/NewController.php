<?php

namespace Nitra\InformationBundle\Controller\Information;

use Admingenerated\NitraInformationBundle\BaseInformationController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param string|FormTypeInterface $type    The built type of the form
     * @param mixed                    $data    The initial data for the form
     * @param array                    $options Options for the form
     *
     * @return Form
     */
    public function createForm($type, $data = null, array $options = array())
    {
        $session = $this->getRequest()->getSession();
        $options = array_merge($options, array(
            'store_id' => $session->get('store_id')
        ));
        return $this->container->get('form.factory')->create($type, $data, $options);
    }
}