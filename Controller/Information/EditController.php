<?php

namespace Nitra\InformationBundle\Controller\Information;

use Admingenerated\NitraInformationBundle\BaseInformationController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\NashaBazaInformationBundle\Document\Information $Information your \Nitra\NashaBazaInformationBundle\Document\Information object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\NashaBazaInformationBundle\Document\Information $Information)
    {
        foreach ($Information->getInformationProducts() as $prod) {
            echo $prod->getId() . '<br>';
        }
    }

    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param string|FormTypeInterface $type    The built type of the form
     * @param mixed                    $data    The initial data for the form
     * @param array                    $options Options for the form
     *
     * @return Form
     */
    public function createForm($type, $data = null, array $options = array())
    {
        $session = $this->getRequest()->getSession();
        $options = array_merge($options, array(
            'store_id' => $session->get('store_id')
        ));

        return $this->container->get('form.factory')->create($type, $data, $options);
    }

    public function postSave(\Symfony\Component\Form\Form $form, $Information)
    {
        $session = $this->getRequest()->getSession();
        $storeId = $session->get('store_id');
        $mongoDbName = $this->container->getParameter('mongo_database_name');
        $storeHost   = $this->container->get('doctrine_mongodb.odm.document_manager')->getRepository('NitraStoreBundle:Store')->find($storeId)->getHost();
        $cache       = $this->getCache();
        $cache->delete($mongoDbName . '_nitra_nice_informations_urls_' . $storeHost);
    }

    /**
     * @return \Doctrine\Common\Cache\ApcCache
     */
    private function getCache()
    {
        return $this->container->get('cache_apc');
    }
}