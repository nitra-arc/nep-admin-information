# InformationBundle

## Описание

данный бандл предназначен для:

* **создания и редактирования категорий статей и самих статей**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-informationbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\InformationBundle\NitraInformationBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
nitra_information:
    resource:    "@NitraInformationBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
informationCategory:
    translateDomain: 'menu'
    route: Nitra_InformationBundle_InformationCategory_list
information:
    translateDomain: 'menu'
    route: Nitra_InformationBundle_Information_list
```